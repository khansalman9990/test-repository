package com.example.demo.microservicesdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.example.demo")
public class MicroservicesDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicesDemoApplication.class, args);
	}

}
