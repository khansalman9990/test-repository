package com.example.demo.repository;

import java.util.List;

import com.example.demo.bean.Student;

public interface StudentRepo {

	public List<Student> getAllStudents();

	public void addStudent(Student student);
}
