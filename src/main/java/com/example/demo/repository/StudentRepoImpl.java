package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.bean.Student;

@Repository
public class StudentRepoImpl implements StudentRepo{
	
	List<Student> list = new ArrayList<>();
	public StudentRepoImpl() {
		list.add(new Student(101,"Salman","Bhopal"));
		list.add(new Student(201,"Amit","Delhi"));
		list.add(new Student(301,"Aman","Mumbai"));
		list.add(new Student(401,"Moshin","Nagpur"));
	}
	
	@Override
	public List<Student> getAllStudents() {
		return list;
	}

	@Override
	public void addStudent(Student student) {
		list.add(student);
		
	}

	
}
